Sutty Data Role
===============

Creates data directories, useful for holding all data under a single
directory, simplifying backups and shared container volumes.

Role Variables
--------------

| Variable | Description                                             | Default value |
| -------- | ------------------------------------------------------- | ------------- |
| `data`   | Main data directory under which directories are created | `/srv/sutty`  |
| `owner`  | Directory owner                                         | `root`        |
| `group`  | Directory group                                         | `root`        |
| `mode`   | Directory mode                                          | `750`         |
| `path`   | Directory to create                                     | -             |

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_data"
    vars:
      path: "/var/lib/postgresql"
```

License
-------

MIT-Antifa
